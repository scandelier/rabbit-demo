<?php
/**
 * Here we receive the sent file, put it in the "input" folder,
 * and send a message to RabbitMQ
 */

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

$uploaddir = '/var/www/input/';

// Check if a file has been uploaded
if (isset($_FILES['userfile'])) {

    // Transfer file in /input
    $filename = basename($_FILES['userfile']['name']);
    $uploadfile = $uploaddir . $filename;
    $message = "";

    if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
        $message = "Fichier téléchargé avec succès : " . basename($_FILES['userfile']['name']);

        // Create a connection to the RabbitMQ server
        $connection = new AMQPStreamConnection('rabbit', 5672, 'guest', 'guest');
        $channel = $connection->channel();

        // Declare a queue
        $channel->queue_declare('files2process', false, false, false, false);

        // Build message and send it (publish)
        $msg = new AMQPMessage(json_encode([
            "filename" => $filename
        ]));

        $channel->basic_publish($msg, '', 'files2process');

        // Close the channel and the connection
        $channel->close();
        $connection->close();

        $message .= "<br>Message envoyé à RabbitMQ.";

    } else {
        $message = "Fichier invalide !";
    }
}

/**
 * And Here we just find the last uploaded image and processed image
 */

$lastInputFileBase = trim(shell_exec("ls -t1 $uploaddir |head -1"));
$lastInputFile =  $uploaddir . $lastInputFileBase ;
$lastProcessedFile = "/var/www/output/" . $lastInputFileBase ;
if (!file_exists($lastProcessedFile))
    $lastProcessedFile = "";

/**
 * Get base 64 content from an image file
 */

function data_uri($file)
{
    if (empty($file))
        return '';

    $contents = file_get_contents($file);
    $base64   = base64_encode($contents);
    $mime = "image/".pathinfo($file,PATHINFO_EXTENSION);
    return ('data:' . $mime . ';base64,' . $base64);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Démo Upload de fichiers, RabbitMQ, traitement par container</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <!-- Refresh the page every 20s; ATTENTION: don't use in production !!!! Awful practice !!! Use Ajax requests instead -->
    <meta http-equiv="refresh" content="20;/">
</head>

<body>
<div class="container mt-5">

    <h1>Démo Upload de fichiers, RabbitMQ, traitement par container</h1>

    <h2 class="mt-5">Envoyer une image</h2>
    <?php
        if (isset($message)) {
            ?>
            <div class="alert alert-warning">
                <?php echo($message) ?>
            </div>
            <?php
        }
    ?>

    <div class="mt-2">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group row">
                <label class="col-sm-4 col-form-label text-right required">Envoyez un fichier d'image :</label>
                <div class="col-sm-6">
                    <input name="userfile" type="file" class="form-control" required/>
                    <small class="form-text text-muted">
                        Max 2Mo SVP, et *.jpg ou *.png (pas de vérif faite, mais plantage assuré sinon).
                    </small>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-4"></div>
                <div class="col-sm-8">
                    <input type="submit" class="btn btn-primary" />
                </div>
            </div>
        </form>
    </div>

    <h2 class="mt-5">Dernière image traitée</h2>
    <div class="row mt-4">
        <div class="col-sm-4 offset-sm-2">
            <h4>Avant</h4>
            <img src="<?php echo data_uri($lastInputFile) ?>" width="100%" height="auto">
        </div>
        <div class="col-sm-4 offset-sm-2">
            <h4>Après</h4>
            <img src="<?php echo data_uri($lastProcessedFile)?>" width="100%" height="auto">
        </div>

    </div>

</div>
</body>
</html>

