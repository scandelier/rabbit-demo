<?php
/**
 * This script connects to RabbitMQ and waits for messages.
 * It then consume them.
 */

require_once __DIR__ . '/vendor/autoload.php';
use PhpAmqpLib\Connection\AMQPStreamConnection;

// Create a connection to the RabbitMQ server (wait 10 seconds before restarting...)
for ($i=0; $i<10; $i++) {
    try {
        $connection = new AMQPStreamConnection('rabbit', 5672, 'guest', 'guest');
        break;
    } catch (Exception $e) {
        if ($i == 9) {
            echo ' [x] Connection to the rabbit container failed... Exiting !', "\n";
            exit(1);
        } else {
            echo ' [x] Connection to the rabbit container failed... retrying in 10 seconds...', "\n";
            sleep(10);
        }
    }
}
$channel = $connection->channel();

// Declare the same queue that in the producer
$channel->queue_declare('files2process', false, false, false, false);

// Callback function running actions based on message
// That is where we call the other container wich
// will perform the actual processing.
$callback = function ($msg) {
    echo ' [x] Received ', $msg->body, "\n";

    $parameters = json_decode($msg->body, true);
    $filename = $parameters['filename'];

    // Artificially wait for a long time
    sleep(15);

    // Process the image file (with imagemagick)
    shell_exec("docker exec imagemagick convert input/$filename -set colorspace sRGB -sepia-tone 80% output/$filename");

    echo " [x] Image converted in output/$filename\n";
};

// Waits for messages
echo " [*] Waiting for messages. To exit press CTRL+C\n";

$channel->basic_consume('files2process', '', false, true, false, false, $callback);
while (count($channel->callbacks)) {
    $channel->wait();
    echo " [*] Waiting for messages, in the loop...\n";
}

// Close channel and connection
// Waits for messages
echo " [*] Closing connection. Bye.\n";
$channel->close();
$connection->close();