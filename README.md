# RabbitMQ Demo 

This project is a demo of how to use RabbitMQ to process uploaded files. 

The project manages the following services via Docker (see the `docker-compose.yml`): 

* `frontend`: the web frontend (php) where you can upload image files, and which sends 
messages to RabbitMQ.
* `rabbit`: a RabbitMQ instance, with one queue where the frontend send messages 
when a new file is uploaded. 
* `consumer`: an instance with a php script, receiving and consuming the messages form the 
RabbitMQ queue. For each uploaded file, it exec a command on the imagemagick container.
* `imagemagick`: the 'processing' container. It is just an example of a toolbox which can 
process the input files. 

## Prerequisites

Docker, docker-compose (version 3), and git. Everything else included in the dockers.

## Installation

Clone the project, then run the `init.sh` script to build it:
```
git clone https://gitlab.com/scandelier/rabbit-demo.git
cd rabbit-demo
./init.sh
```

## Usage 

Launch the dockers with compose: 
```
cd rabbit-demo
chmod -R 777 files

docker-compose build
docker-compose run frontend composer install
docker-compose run consumer composer install

docker-compose up
```
Visit the frontend at http://localhost:8123.

Upload some image files. Uploaded files will be put in `files/input`. 
Processed files (a sepia filter is applied) will be put in `files/output`.

The RabbitMQ Manager is accessible at http://localhost:15672 (guest/guest).