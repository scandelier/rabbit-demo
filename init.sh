#!/usr/bin/env bash

# Permissions
chmod -R 777 files

# Build and start Docker containers, then run composer to install dependencies
docker-compose build
docker-compose up -d
docker-compose exec frontend composer install
docker-compose exec consumer composer install
docker-compose stop

# Instructions
echo "---"
echo "Project has been built...."
echo "Now run:"
echo "docker-compose up"
echo "Visit http://localhost:8123"
